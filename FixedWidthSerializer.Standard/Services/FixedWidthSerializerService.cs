﻿using FixedWidthSerializer.Extensions;
using FixedWidthSerializer.Models;
using FixedWidthSerializer.Models.Validation;
using FixedWidthSerializer.Repositories;
using FixedWidthSerializer.Validators;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FixedWidthSerializer.Services
{
    internal class FixedWidthSerializerService : IFixedWidthSerializerService
    {
        private IAttributeRepository AttributeRepository { get; }
        private FixedWidthValidator FixedWidthValidator { get; }

        public FixedWidthSerializerService() : this(new AttributeRepository(), new FixedWidthValidator())
        {

        }

        public FixedWidthSerializerService(FixedWidthValidator fixedWidthValidator) : this(new AttributeRepository(), fixedWidthValidator)
        {

        }

        public FixedWidthSerializerService(IAttributeRepository attributeRepository, FixedWidthValidator fixedWidthValidator)
        {
            AttributeRepository = attributeRepository;
            FixedWidthValidator = fixedWidthValidator;
        }


        /// <summary>
        /// Validates the model against the fixed width validator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ValidationResult ValidateModel(object model)
        {
            var result = FixedWidthValidator.Validate(model);
            return new ValidationResult(result.Errors);
        }

        /// <summary>
        /// Validates the model against the fixed width validator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ValidationResult> ValidateModelAsync(object model)
        {
            var results = await FixedWidthValidator.ValidateAsync(model);
            return new ValidationResult(results.Errors);
        }


        /// <summary>
        /// Serializes the object according to the attribute rules
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string Serialize(object model)
        {
            return AttributeRepository.GetAttributeAndPropertyInformation(model)
                        .Select(a => new Value(model, a))
                        .Select(a => a.FormattedPropertyValue.ExpandString(a.Info.Attribute.Length))
                        .Aggregate((working, next) => string.Concat(working, next));
        }


        /// <summary>
        /// Deserialzies the string to the object according to the attribute rules
        /// </summary>
        /// <typeparam name="T"> Type parameter for the return type</typeparam>
        /// <param name="data"> The fixed width string</param>
        /// <returns></returns>
        public T Deserialize<T>(string data)
        {
            var returnObject = Activator.CreateInstance<T>();

            var attributes = AttributeRepository.GetAttributeAndPropertyInformation<T>().OrderBy(a => a.Attribute.Position).ToArray();
            var positions = attributes.Select(a => a.Attribute.Length).ToArray();
            var lengths = data.SplitString(positions).Select(s => s.TrimEnd()).Select(s => string.IsNullOrWhiteSpace(s) ? null : s).ToArray();


            for (var i = 0; i < lengths.Length; i++)
            {
                var currentAttribute = attributes[i];

                try
                {
                    var value = FixedWidthFormatter.FormatStringToObject(lengths[i], currentAttribute.Attribute.FormatString, currentAttribute.PropertyInfo.PropertyType);
                    typeof(T).GetProperty(currentAttribute.PropertyInfo.Name).SetValue(returnObject, value);
                }
                catch (FormatException e)
                {
                    var errorMessage = $"The property {currentAttribute.PropertyInfo.Name} with value \"{lengths[i]}\" was unable to be converted into {currentAttribute.PropertyInfo.PropertyType}";
                    var exception = new FormatException(errorMessage, e);
                    throw exception;
                }
            }

            return returnObject;
        }
    }
}
