﻿using FixedWidthSerializer.Models.Validation;
using System.Threading.Tasks;

namespace FixedWidthSerializer.Services
{
    internal interface IFixedWidthSerializerService
    {
        T Deserialize<T>(string data);
        string Serialize(object model);
        ValidationResult ValidateModel(object model);
        Task<ValidationResult> ValidateModelAsync(object model);
    }
}