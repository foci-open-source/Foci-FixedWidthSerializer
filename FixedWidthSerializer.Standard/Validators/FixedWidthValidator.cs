﻿using FixedWidthSerializer.Attributes;
using FixedWidthSerializer.Models;
using FixedWidthSerializer.Repositories;
using FluentValidation;
using System.Linq;

namespace FixedWidthSerializer.Validators
{
    internal class FixedWidthValidator : AbstractValidator<object>
    {
        private IAttributeRepository AttributeRepository { get; }

        public FixedWidthValidator() : this(new AttributeRepository())
        {

        }

        public FixedWidthValidator(IAttributeRepository attributeRepository)
        {
            AttributeRepository = attributeRepository;

            RuleFor(o => o).Must(HaveFixedWidthAttribute).WithMessage("No FixedWidthColumnAttribute was applied to the object being validated");
            RuleFor(o => o).Must(NotHaveDuplicatePosition).WithMessage("The object has a duplicate priority value.");

            RuleFor(o => o).Must(HaveAppropriateLengthValues).WithMessage(model => string.Format("The following properties have a value whos size is greater then the defined length. {0}", GetInvalidPropertyNames(model)));

        }

        /// <summary>
        /// Validates if the the object has any of the <see cref="FixedWidthColumnAttribute"/> attached to it.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool HaveFixedWidthAttribute(object model)
        {
            var attributes = AttributeRepository.GetColumnAttributes(model);
            return attributes.Any();
        }

        /// <summary>
        /// Ensures that no duplicate positions are applied to the model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool NotHaveDuplicatePosition(object model)
        {
            var attributes = AttributeRepository.GetColumnAttributes(model);
            return attributes.Distinct().Count() == attributes.Count();
        }

        /// <summary>
        /// Ensures that the data stored in the object is not longer then the defined lengths
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool HaveAppropriateLengthValues(object model)
        {
            return AttributeRepository.GetAttributeAndPropertyInformation(model)
                                           .Select(a => new Value(model, a))
                                           .All(a => !a.IsValueLongerThenLength);
        }

        /// <summary>
        /// Helper method to find the property names that have failed the length validation.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string GetInvalidPropertyNames(object model)
        {
            var values = AttributeRepository.GetAttributeAndPropertyInformation(model)
                .Select(a => new Value(model, a))
                .Where(a => a.IsValueLongerThenLength)
                .Select(a => string.Concat(a.Info.PropertyInfo.Name));

            return string.Join(": ", values);
        }
    }
}
