﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixedWidthSerializer.Models.Validation
{
    [Serializable]
    public class ValidationResult
    {

        private readonly IList<ValidationFailure> errors;

        /// <summary>
        /// Whether validation succeeded
        /// </summary>
        public virtual bool IsValid => Errors.Count == 0;

        /// <summary>
        /// A collection of errors
        /// </summary>
        public IList<ValidationFailure> Errors => errors;

        /// <summary>
        /// Creates a new validationResult
        /// </summary>
        public ValidationResult()
        {
            this.errors = new List<ValidationFailure>();
        }

        /// <summary>
        /// Creates a new ValidationResult from a collection of failures
        /// </summary>
        /// <param name="failures">List of <see cref="ValidationFailure"/> which is later available through <see cref="Errors"/>. This list get's copied.</param>
        /// <remarks>
        /// Every caller is responsible for not adding <c>null</c> to the list.
        /// </remarks>
        public ValidationResult(IEnumerable<ValidationFailure> failures)
        {
            errors = failures.Where(failure => failure != null).ToList();
        }

        /// <summary>
        /// Creates a new ValidationResult from a collection of failures
        /// </summary>
        /// <param name="failures">List of <see cref="FluentValidation.Results.ValidationFailure"/> which is later available through <see cref="Errors"/>. This list get's copied.</param>
        /// <remarks>
        /// Every caller is responsible for not adding <c>null</c> to the list.
        /// </remarks>
        public ValidationResult(IEnumerable<FluentValidation.Results.ValidationFailure> failures)
        {
            errors = failures.Where(failure => failure != null).Select(failure => new ValidationFailure(failure)).ToList();
        }
    }
}