﻿using FixedWidthSerializer.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FixedWidthSerializer.Models
{
    public class FixedWidthAttributedAndPropertyInfo
    {
        public FixedWidthAttributedAndPropertyInfo(FixedWidthColumnAttribute attribute, PropertyInfo propertyInfo)
        {
            this.Attribute = attribute;
            this.PropertyInfo = propertyInfo;
        }

        public FixedWidthColumnAttribute Attribute { get; }
        public PropertyInfo PropertyInfo { get; }
    }
}
