﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace FixedWidthSerializer.Models
{
    public static class FixedWidthFormatter
    {

        private static readonly IDictionary<string, Func<object, string, string>> typeToString;
        private static readonly IDictionary<string, Func<string, string, object>> stringToType;

        static FixedWidthFormatter()
        {
            typeToString = new Dictionary<string, Func<object, string, string>>
            {
                { typeof(DateTime).FullName, (value, format) => DateTimeToString((DateTime)value, format)},
                { typeof(DateTimeOffset).FullName, (value, format) => DateTimeOffsetToString((DateTimeOffset)value, format)},
                { typeof(DateTime?).FullName, (value, format) => DateTimeToString((DateTime?)value, format)},
                { typeof(DateTimeOffset?).FullName, (value, format) => DateTimeOffsetToString((DateTimeOffset?)value, format)},
            };

            stringToType = new Dictionary<string, Func<string, string, object>>
            {
                { typeof(DateTime).FullName, (value, format) => StringToDateTime(value, format)},
                { typeof(DateTimeOffset).FullName, (value, format) => StringToDateTimeOffset(value, format)},
                { typeof(DateTime?).FullName, (value, format) => StringToNullableDateTime(value, format)},
                { typeof(DateTimeOffset?).FullName, (value, format) => StringToNullableDateTimeOffset(value, format)},
            };
        }

        public static string FormatObjectToString(object value)
        {
            return FormatObjectToString(value, string.Empty);
        }

        public static string FormatObjectToString(object value, string format)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var typeName = value.GetType().FullName;

            if (typeToString.ContainsKey(typeName))
            {
                return typeToString[typeName].Invoke(value, format);
            }

            return value is IFormattable
                    ? DefaultFormattableObjectToString((IFormattable)value, format)
                    : DefaultObjectToString(value);

        }

        public static object FormatStringToObject(string value, Type type)
        {
            return FormatStringToObject(value, string.Empty, type);
        }

        public static object FormatStringToObject(string value, string format, Type type)
        {
            var typeName = type.FullName;

            return stringToType.ContainsKey(typeName)
                        ? stringToType[typeName].Invoke(value, format)
                        : DefaultStringToObject(value, type);
        }

        private static string DefaultObjectToString(object value)
        {
            return value.ToString();
        }

        private static T DefaultStringToObject<T>(string value)
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }

        private static string DefaultFormattableObjectToString(IFormattable value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        private static T DefaultStringToFormattableObject<T>(string value) where T : IFormattable
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }

        private static object DefaultStringToObject(string value, Type type)
        {
            if (value == null)
            {
                return GetDefault(type);
            }

            var conversionType = Nullable.GetUnderlyingType(type) ?? type;

            return Convert.ChangeType(value, conversionType);
        }

        private static DateTime StringToDateTime(string value, string format)
        {
            if (string.IsNullOrEmpty(value))
            {
                return DateTime.MinValue;
            }

            return DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
        }

        private static DateTime? StringToNullableDateTime(string value, string format)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
        }

        private static DateTimeOffset StringToDateTimeOffset(string value, string format)
        {
            if (string.IsNullOrEmpty(value))
            {
                return DateTimeOffset.MinValue;
            }

            return DateTimeOffset.ParseExact(value, format, CultureInfo.InvariantCulture);
        }

        private static DateTimeOffset? StringToNullableDateTimeOffset(string value, string format)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return DateTimeOffset.ParseExact(value, format, CultureInfo.InvariantCulture);
        }

        private static string DateTimeToString(DateTime value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        private static string DateTimeToString(DateTime? value, string format)
        {
            if (!value.HasValue)
            {
                return string.Empty;
            }

            return value.Value.ToString(format, CultureInfo.InvariantCulture);
        }

        private static string DateTimeOffsetToString(DateTimeOffset value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        private static string DateTimeOffsetToString(DateTimeOffset? value, string format)
        {
            if (!value.HasValue)
            {
                return string.Empty;
            }

            return value.Value.ToString(format, CultureInfo.InvariantCulture);
        }

        private static string DecimalToString(decimal value, string format)
        {
            return value.ToString(format, CultureInfo.InvariantCulture);
        }

        private static object GetDefault(Type t)
        {
            Func<object> f = GetDefault<object>;
            return f.Method.GetGenericMethodDefinition().MakeGenericMethod(t).Invoke(null, null);
        }

        private static T GetDefault<T>()
        {
            return default(T);
        }

    }
}
