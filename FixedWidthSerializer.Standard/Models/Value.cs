﻿using FixedWidthSerializer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixedWidthSerializer.Models
{
    public class Value
    {
        public FixedWidthAttributedAndPropertyInfo Info { get; }        
        private object Model { get; }

        public Value(object model, FixedWidthAttributedAndPropertyInfo info)
        {
            this.Info = info;
            this.Model = model;
        }

        public object PropertyValue {
            get
            {
                return Info.PropertyInfo.GetValue(Model);
            }
        }

        public string FormattedPropertyValue
        {
            get
            {
                return FixedWidthFormatter.FormatObjectToString(this.PropertyValue, this.Info.Attribute.FormatString);               
            }
        }

        public bool IsValueLongerThenLength
        {
            get
            {
                return FormattedPropertyValue.Length > Info.Attribute.Length;
            }
        }
    }
}
