﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FixedWidthSerializer.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    public sealed class FixedWidthColumnAttribute : Attribute
    {
        public FixedWidthColumnAttribute(int position, int length)
        {
            this.Position = position;
            this.Length = length;
            this.ValidationMessage = string.Empty;
            this.FormatString = string.Empty;
        }        

        public FixedWidthColumnAttribute(int position, int length, string formatString = "", string validationMessage = "") : this(position, length)
        {
            this.ValidationMessage = validationMessage ?? string.Empty;
            this.FormatString = formatString ?? string.Empty;
        }

        public int Position { get; }
        public int Length { get; }
        public string ValidationMessage { get; }
        public string FormatString { get; }
    }
}
