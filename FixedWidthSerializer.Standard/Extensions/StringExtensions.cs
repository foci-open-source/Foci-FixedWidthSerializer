﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FixedWidthSerializer.Extensions
{
    public static class StringExtensions
    {
        public static string ExpandString(this string str, int length)
        {
            if (length <= str.Length)
            {
                return str.Substring(0, length);
            }

            return str.PadRight(length);
        }

        public static IEnumerable<string> SplitString(this string line, params int[] lengths)
        {
            if (lengths.Sum() > line.Length)
            {
                var errorMessage = $"The line provided was too short for the given lengths. Verify that the line is well formatted.";
                throw new FormatException(errorMessage);
            }

            var parts = new string[lengths.Length];

            var startPos = 0;
            for (var i = 0; i < lengths.Length; i++)
            {
                parts[i] = line.Substring(startPos, lengths[i]);
                startPos += lengths[i];
            }

            return parts;
        }
    }
}
