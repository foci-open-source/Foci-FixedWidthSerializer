﻿using System.Collections.Generic;
using System.Reflection;
using FixedWidthSerializer.Attributes;
using FixedWidthSerializer.Models;

namespace FixedWidthSerializer.Repositories
{
    internal interface IAttributeRepository
    {
        /// <summary>
        /// Gets the column attributes for the given model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IEnumerable<FixedWidthAttributedAndPropertyInfo> GetAttributeAndPropertyInformation(object model);
        /// <summary>
        /// Get the column attributes for the type of T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IEnumerable<FixedWidthAttributedAndPropertyInfo> GetAttributeAndPropertyInformation<T>();
        /// <summary>
        /// Gets <see cref="PropertyInfo"/> model whos properties have the column attribute
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IEnumerable<FixedWidthColumnAttribute> GetColumnAttributes(object model);
        /// <summary>
        /// Gets <see cref="PropertyInfo"/> model whos properties have the column attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IEnumerable<FixedWidthColumnAttribute> GetColumnAttributes<T>();
        /// <summary>
        /// Gets the <see cref="FixedWidthColumnAttribute"/> and <see cref="PropertyInfo"/> for the propeties with the <see cref="FixedWidthColumnAttribute"/>
        /// </summary>
        /// <param name="model"></param>
        /// <returns><see cref="FixedWidthAttributedAndPropertyInfo"/></returns>
        IEnumerable<PropertyInfo> GetPropertiesWithAttribute(object model);
        /// <summary>
        /// Gets the <see cref="FixedWidthColumnAttribute"/> and <see cref="PropertyInfo"/> for the propeties with the <see cref="FixedWidthColumnAttribute"/>
        /// </summary>
        /// <param name="model"></param>
        /// <returns><see cref="FixedWidthAttributedAndPropertyInfo"/></returns>
        IEnumerable<PropertyInfo> GetPropertiesWithAttribute<T>();
    }
}