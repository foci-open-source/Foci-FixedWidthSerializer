﻿using FixedWidthSerializer.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;
using FixedWidthSerializer.Models;

namespace FixedWidthSerializer.Repositories
{
    internal class AttributeRepository : IAttributeRepository
    {
        /// <summary>
        /// Gets the column attributes for the given model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<FixedWidthColumnAttribute> GetColumnAttributes(object model)
        {
            return model.GetType()
                        .GetTypeInfo()
                        .GetProperties()
                        .Select(p => p.GetCustomAttribute<FixedWidthColumnAttribute>());
        }

        /// <summary>
        /// Get the column attributes for the type of T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<FixedWidthColumnAttribute> GetColumnAttributes<T>()
        {
            return GetColumnAttributes(Activator.CreateInstance<T>());
        }

        /// <summary>
        /// Gets <see cref="PropertyInfo"/> model whos properties have the column attribute
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<PropertyInfo> GetPropertiesWithAttribute(object model)
        {
            return model.GetType()
                        .GetTypeInfo()
                        .DeclaredProperties
                        .Where(x => x.IsDefined(typeof(FixedWidthColumnAttribute)));
        }

        /// <summary>
        /// Gets <see cref="PropertyInfo"/> model whos properties have the column attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<PropertyInfo> GetPropertiesWithAttribute<T>()
        {
            return this.GetPropertiesWithAttribute(Activator.CreateInstance<T>());
        }


        /// <summary>
        /// Gets the <see cref="FixedWidthColumnAttribute"/> and <see cref="PropertyInfo"/> for the propeties with the <see cref="FixedWidthColumnAttribute"/>
        /// </summary>
        /// <param name="model"></param>
        /// <returns><see cref="FixedWidthAttributedAndPropertyInfo"/></returns>
        public IEnumerable<FixedWidthAttributedAndPropertyInfo> GetAttributeAndPropertyInformation(object model)
        {
            return this.GetPropertiesWithAttribute(model)
                       .Select(pi => new FixedWidthAttributedAndPropertyInfo(pi.GetCustomAttribute<FixedWidthColumnAttribute>(), pi));
        }

        /// <summary>
        /// Gets the <see cref="FixedWidthColumnAttribute"/> and <see cref="PropertyInfo"/> for the propeties with the <see cref="FixedWidthColumnAttribute"/>
        /// </summary>
        /// <param name="model"></param>
        /// <returns><see cref="FixedWidthAttributedAndPropertyInfo"/></returns>
        public IEnumerable<FixedWidthAttributedAndPropertyInfo> GetAttributeAndPropertyInformation<T>()
        {
            return this.GetAttributeAndPropertyInformation(Activator.CreateInstance<T>());
        }

    }
}
