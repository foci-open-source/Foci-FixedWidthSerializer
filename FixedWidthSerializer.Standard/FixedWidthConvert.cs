﻿using FixedWidthSerializer.Models.Validation;
using FixedWidthSerializer.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FixedWidthSerializer
{
    public static class FixedWidthConvert
    {
        /// <summary>
        /// Serializes the given models
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IEnumerable<string> Serialize<T>(IEnumerable<T> models)
        {
            return models.Select(m => Serialize(m));
        }

        /// <summary>
        /// Serializes the given model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string Serialize(object model)
        {
            var service = new FixedWidthSerializerService();
            return service.Serialize(model);
        }

        /// <summary>
        /// Deserializes a list of strings to the type defined as the generic parameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static IEnumerable<T> Deserialize<T>(IEnumerable<string> lines)
        {
            return lines.Select(Deserialize<T>);
        }

        /// <summary>
        /// Deserializes the string the type defined as the generic paramerter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="line"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string line)
        {
            var service = new FixedWidthSerializerService();
            return service.Deserialize<T>(line);
        }

        /// <summary>
        /// Validates the model as per the rules in the FixedWidthValidator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ValidationResult Validate(object model)
        {
            var service = new FixedWidthSerializerService();
            return service.ValidateModel(model);
        }


        /// <summary>
        /// Validates the model as per the rules in the FixedWidthValidator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<ValidationResult> ValidateAsync(object model)
        {
            var service = new FixedWidthSerializerService();
            return await service.ValidateModelAsync(model);
        }
    }
}
