﻿using FixedWidthSerializer.Attributes;
using System;

namespace FixedWidthSerializer.Standard.Tests.TestingClasses
{
    public class TestSegment
    {
        public TestSegment()
        {
            Segnam = "FirstLineSeg";
        }

        public int LineNumber { get; set; }

        /// <summary>
        /// Segment Name
        /// default to RCPT_TRUCK_SEG
        /// </summary>
        [FixedWidthColumn(4, 20)]
        public string Segnam { get; set; }

        /// <summary>
        /// Transaction Type
        /// default to A
        /// </summary>
        [FixedWidthColumn(5, 1)]
        public string Trntyp { get; set; }

        /// <summary>
        /// Truck Number
        /// default to Receipt Number
        /// </summary>
        [FixedWidthColumn(6, 20)]
        public string Trknum { get; set; }

        /// <summary>
        /// Truck Reference
        /// </summary>
        [FixedWidthColumn(7, 20)]
        public string Trkref { get; set; }

        /// <summary>
        /// Truck Status
        /// default to E - expected
        /// </summary>
        [FixedWidthColumn(8, 1)]
        public string Trksts { get; set; }

        /// <summary>
        /// Transport Method
        /// </summary>
        [FixedWidthColumn(9, 1)]
        public string Trnspt { get; set; }

        /// <summary>
        /// Number of Pallets
        /// </summary>
        [FixedWidthColumn(10, 10)]
        public int? Numpal { get; set; }

        /// <summary>
        /// Number of Cases
        /// </summary>
        [FixedWidthColumn(11, 10)]
        public long? Numcas { get; set; }

        /// <summary>
        /// Gross Weight
        /// </summary>
        [FixedWidthColumn(12, 12)]
        public long? Grswgt { get; set; }

        /// <summary>
        /// Freight Cost
        /// </summary>
        [FixedWidthColumn(13, 12)]
        public long? Frtcst { get; set; }

        /// <summary>
        /// Shipment Date
        /// </summary>
        [FixedWidthColumn(14, 8, formatString: "yyyyMMdd")]
        public DateTime? Shpdte { get; set; }

        /// <summary>
        /// Expected Date
        /// </summary>
        [FixedWidthColumn(15, 8, formatString: "yyyyMMdd")]
        public DateTime? Expdte { get; set; }

        /// <summary>
        /// Arrive Date
        /// </summary>
        [FixedWidthColumn(16, 8, formatString: "yyyyMMdd")]
        public DateTime? Arrdte { get; set; }

        /// <summary>
        /// Receiving Dock
        /// </summary>
        [FixedWidthColumn(17, 20)]
        public string Rcvdck { get; set; }

        /// <summary>
        /// Device Code
        /// </summary>
        [FixedWidthColumn(18, 10)]
        public string Devcod { get; set; }

        /// <summary>
        /// Produce Labels Flag
        /// </summary>
        [FixedWidthColumn(19, 1)]
        public string Lblflg { get; set; }

        /// <summary>
        /// same as Receipt Number
        /// </summary>
        [FixedWidthColumn(20, 20)]
        public string UcRcpnum { get; set; }
    }
}
