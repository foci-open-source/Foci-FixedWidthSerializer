﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixedWidthSerializerTests.TestingClasses
{
    public class NoParametersTestClass
    {
        public int Integrer { get; set; }
        public decimal Decimal { get; set; }
        public float Float { get; set; }
        public DateTime Datetime { get; set; }
        public string String { get; set; }

        public NoParametersTestClass(int Integrer, decimal Decimal, float Float, DateTime Datetime, string String)
        {
            this.Integrer = Integrer;
            this.Decimal = Decimal;
            this.Float = Float;
            this.Datetime = Datetime;
            this.String = String;
        }
    }
}
