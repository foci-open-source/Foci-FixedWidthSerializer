﻿using FixedWidthSerializer.Attributes;
using System;

namespace FixedWidthSerializerTests.TestingClasses
{
    public class BasicTestClass : IEquatable<BasicTestClass>
    {
        [FixedWidthColumn(1, 5)]
        public int Integrer { get; set; }
        [FixedWidthColumn(2, 10, formatString: "#.##")]
        public decimal Decimal { get; set; }
        [FixedWidthColumn(3, 5)]
        public float Float { get; set; }
        [FixedWidthColumn(4, 10, formatString: "MM/dd/yyyy")]
        public DateTime Datetime { get; set; }
        [FixedWidthColumn(5, 5)]
        public string String { get; set; }

        public BasicTestClass(int Integrer, decimal Decimal, float Float, DateTime Datetime, string String)
        {
            this.Integrer = Integrer;
            this.Decimal = Decimal;
            this.Float = Float;
            this.Datetime = Datetime;
            this.String = String;
        }

        public BasicTestClass()
        {

        }

        public override bool Equals(object obj)
        {
            return Equals(obj as BasicTestClass);
        }

        public bool Equals(BasicTestClass other)
        {
            return other != null &&
                   Integrer == other.Integrer &&
                   Decimal == other.Decimal &&
                   Float == other.Float &&
                   Datetime == other.Datetime &&
                   String == other.String;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Integrer, Decimal, Float, Datetime, String);
        }
    }
}
