using FixedWidthSerializer.Standard.Tests.TestingClasses;
using FixedWidthSerializerTests.TestingClasses;
using System;
using System.Collections.Generic;
using Xunit;

namespace FixedWidthSerializer.Standard.Tests
{
    public class BasicImplemenationTests
    {
        [Fact]
        public void VerifyComplexObjectSuccess()
        {
            var input = new
            {
                line = "1234_12345_SEG      ATSHP000024                              E                                             2018121820181217                                       TSHP000024          "
            };

            var expected = input.line;
            var intermediate = FixedWidthConvert.Deserialize<TestSegment>(input.line);
            var actual = FixedWidthConvert.Serialize(intermediate);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerifyWithNullNullableSuccess()
        {
            var input = new
            {
                lines = new List<string>()
                {
                    string.Concat("1234_12345_SEG      ",
                                      "A",
                                      "000040              ",
                                      "56789012345         ",
                                      "E",
                                      " ",
                                      "          ",
                                      "11        ",
                                      "594         ",
                                      "0           ",
                                      "20180807",
                                      "20180808",
                                      "        ",
                                      "                    ",
                                      "          ",
                                      " ",
                                      "000040              "
                                      ),
                }
            };

            var expected = input.lines[0];
            var intermediate = FixedWidthConvert.Deserialize<TestSegment>(input.lines[0]);
            var actual = FixedWidthConvert.Serialize(intermediate);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DeserializeTestSuccess()
        {
            var datetime = DateTime.Now;
            var date = datetime.ToString("MM/dd/yyyy");

            var input = new
            {
                line = string.Concat("1234512.36     1.124", date, "Hello")
            };

            var expected = new BasicTestClass(12345, 12.36M, 1.124F, new DateTime(datetime.Year, datetime.Month, datetime.Day), "Hello");
            var actual = FixedWidthConvert.Deserialize<BasicTestClass>(input.line);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DeserializeTest2Success()
        {
            var datetime = DateTime.Now;
            var date = datetime.ToString("MM/dd/yyyy");

            var input = new
            {
                line = new List<string>()
                            {
                                 string.Concat("1234512.36     1.124", date, "Hello"),
                                 string.Concat("1234512.36     1.124", date, "Hello")
                            }
            };

            var expected = new List<BasicTestClass>()
            {
                new BasicTestClass(12345, 12.36M, 1.124F, new DateTime(datetime.Year, datetime.Month, datetime.Day), "Hello"),
                new BasicTestClass(12345, 12.36M, 1.124F, new DateTime(datetime.Year, datetime.Month, datetime.Day), "Hello")
            };

            var actual = FixedWidthConvert.Deserialize<BasicTestClass>(input.line);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SerializeTestSuccess()
        {
            var input = new
            {
                testClass = new BasicTestClass(12345, 12.3623M, 1.124F, DateTime.Now, "Hello")
            };

            var date = DateTime.Now.ToString("MM/dd/yyyy");
            var expected = string.Concat("1234512.36     1.124", date, "Hello");
            var actual = FixedWidthConvert.Serialize(input.testClass);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SerializeTest2Success()
        {
            var input = new
            {
                testClass = new List<BasicTestClass>()
                {
                    new BasicTestClass(12345, 12.3623M, 1.124F, DateTime.Now, "Hello"),
                    new BasicTestClass(12345, 12.3623M, 1.124F, DateTime.Now, "Hello"),
                }
            };

            var date = DateTime.Now.ToString("MM/dd/yyyy");
            var expected = new List<string>()
            {
                string.Concat("1234512.36     1.124", date, "Hello"),
                string.Concat("1234512.36     1.124", date, "Hello"),
            };

            var actual = FixedWidthConvert.Serialize(input.testClass);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SerializeTestFailure()
        {
            var input = new
            {
                testClass = new BasicTestClass(12345, 12.3623M, 1.124F, DateTime.Now, "HelloTOOOOLONG")
            };

            var date = DateTime.Now.ToString("MM/dd/yyyy");
            var expected = string.Concat("1234512.36     1.124", date, "HelloTOOOOLONG");
            var actual = FixedWidthConvert.Serialize(input.testClass);

            Assert.NotEqual(expected, actual);
        }

        [Fact]
        public void ValidationNoParametersTest()
        {
            var input = new
            {
                testClass = new NoParametersTestClass(12345, 12.3623M, 1.124F, DateTime.Now, "hello")
            };

            var actual = FixedWidthConvert.Validate(input.testClass);

            Assert.False(actual.IsValid);
        }
    }
}
